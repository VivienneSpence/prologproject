%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Name: Fruzsina Vivienne Benke   %
% E-mail: fbenke11@student.aau.dk %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
%%%% Map data %%%%
%%%%%%%%%%%%%%%%%%

% A route has a start city and a Destination; distance between the two in km, type of road and travel time in hours.
route(aalborg, randers, 80, motorway, 1).
route(randers, aarhus, 39, motorway, 0.5).
route(randers, tirstrup, 50, paved, 1).
route(tirstrup, trustrup, 8, gravel, 0.13).
route(aalborg, stovring, 27, motorway, 0.4).
route(stovring, hobro, 35, motorway, 0.4).
route(aarhus, copenhagen, 187, motorway, 3).
route(aalborg, gistrup, 13, paved, 0.35).
route(viborg, vejle, 92, motorway, 1.1).
route(aarhus, vejle, 72, motorway, 0.82).
route(vejle, odense, 79, motorway, 0.9).
route(odense, copenhagen, 165, motorway, 1.9).

%%%%%%%%%%%%%%%%%%%%%%%%%
% Road and vehicle data %
%%%%%%%%%%%%%%%%%%%%%%%%%

% Vehicle type and number of passengers
vehicle(bicycle, 1).
vehicle(taxi, 4).
vehicle(car, 4).
vehicle(bus, inf).

% Kind of vehicle
% Checks the type of the vehicle.
isBicycle(bicycle).
isCar(car).
isCar(taxi).
isTaxi(taxi).
isBus(bus).

% Road usage
% What kind of vehicle can use what kind of road.
% Give it a road type and a vehicle and it returns true if the vehicle can use the road and false otherwise.
% Ensures consistency for vehicle
useRoad(motorway, Vehicle):- isBus(Vehicle).
useRoad(motorway, Vehicle):- isCar(Vehicle).
useRoad(paved, Vehicle):- isBicycle(Vehicle).
useRoad(paved, Vehicle):- isCar(Vehicle).
useRoad(paved, Vehicle):- isBus(Vehicle).
useRoad(gravel, Vehicle):- isBicycle(Vehicle).
useRoad(gravel, Vehicle):- isCar(Vehicle). 

%%%%%%%%%%%%%%%%%%
% Traveller data %
%%%%%%%%%%%%%%%%%%

% Traveller has a name and a list of the vehicle he/she can use.
traveller(lindon, [bicycle]).
traveller(yerin, [bicycle, bus]).
traveller(eithan, [car, taxi]).
traveller(mercy, [bicycle, bus, taxi]).
traveller(cassias, [bus, taxi, car]).

% Predicates to check if certain traveller can use a certain vehicle or not.
% Returns true or false.
% Ensures consistency for traveller
canUse(lindon, Vehicle):- isBicycle(Vehicle).
canUse(yerin, Vehicle):- isBicycle(Vehicle).
canUse(yerin, Vehicle):- isBus(Vehicle).
canUse(eithan, Vehicle):- isTaxi(Vehicle) ; isCar(Vehicle).
canUse(mercy, Vehicle):- isBicycle(Vehicle).
canUse(mercy, Vehicle):- isBus(Vehicle).
canUse(mercy, Vehicle):- isTaxi(Vehicle).
canUse(cassias, Vehicle):- isBus(Vehicle).
canUse(cassias, Vehicle):- isCar(Vehicle) ; isTaxi(Vehicle).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% A predicate to make sure that a traveller can travel either way.
% A traveller cannot have the same city as origin and as Destination
way(Origin, Origin, _, _, _):- fail.
way(Origin, Destination, Distance, RoadType, TravelTime):-
   route(Origin, Destination, Distance, RoadType, TravelTime);
   route(Destination, Origin, Distance, RoadType, TravelTime).

% Finds paths between two cities, origin and destination without any criteria
% It uses a helper for non-direct paths
path(Origin, Destination, Route):-
  path_helper(Origin, Destination, [Origin], Route).

% This is a helper to handle the recursive call in case there is no direct route between origin and destination
% It only adds the intermediate town to the list if not already in it
% It also checks that the destination is not in the list  
path_helper(Origin, Destination, CurrentRoute, Result) :-
  way(Origin, Destination, _, _, _), 
  appendToList(CurrentRoute, Destination, Result).
path_helper(Origin, Destination, CurrentRoute, Result) :-
  way(Origin, Intermediate, _, _, _),  
  notInList(Intermediate, CurrentRoute), 
  notInList(Destination, CurrentRoute), 
  appendToList(CurrentRoute, Intermediate, UpdatedCurrentRoute),
  path_helper(Intermediate, Destination, UpdatedCurrentRoute, Result).   

% Gets all available road types the vehicles can use
getRoadTypes(Vehicle, RoadTypes) :-
  setof(Road, Vehicle^useRoad(Road, Vehicle), RoadTypes).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1. Reqirement from miniproject: Find a route between two towns that a given person can travel by, if possible %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% What kind of road can the traveller use
% Get the possible routes based on the road types the traveller can use

personalRoute(Origin, Destination, Traveller, Route):-
   roadTypesForTraveller(Traveller, RoadTypes),
   pathWithRoadTypes(Origin, Destination, RoadTypes, Route).

% Helpers for Requirement 1
% Gets all the road types a traveller can use
% First it gets the list of vehicles the traveller can use
% Based on that creates a list of all the road types the traveller can use
% The '^' operator creates a single list from all the results
roadTypesForTraveller(Traveller, Result) :-
  traveller(Traveller, VehicleList),
  setof(RoadType, RoadType^vehiclesRoadType(VehicleList, RoadType), Result).

% Predicate the tells if a traveller can use a road type between origin and destination
% First it gets the list of vehicles the traveller can use
% Then it finds out the road type between origin and destination
% then it checks if the vehicles in the list can use the obtained road type
canTravellerUseRoad(Traveller, Origin, Destination):-
  traveller(Traveller, VehicleList),
  route(Origin, Destination, _, RoadType, _),
  vehiclesRoadType(VehicleList, RoadType).

% Predicate to see if a list of vehicles can use a certain road type
% It checks if the head of the vehicle list can use the road and then it calls itself recursively on the tail of the list of vehicles
vehiclesRoadType([], _) :- fail.
vehiclesRoadType([Vehicle|RestList], RoadType) :-
  useRoad(RoadType, Vehicle);
  vehiclesRoadType(RestList, RoadType).

% Similar to personalRoute but it does not return a route
% It returns true if the person can travel from origin to destination and false otherwise
% Note, that it returns as many number of true as many route is available
canPersonTravelRoute(Origin, Destination, Traveller):-
   roadTypesForTraveller(Traveller, RoadTypes),
   pathWithRoadTypes(Origin, Destination, RoadTypes, R).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2. Reqirement from miniproject:  Find a route that passes though towns from a given list, if possible %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Finds a route between origin and destination
% Checks if the list of requested Towns is in the found route
% If it is it returns the route
% If it is not it returns false
pathWithTowns(Origin, Destination, Towns, Route):-
   path(Origin, Destination, Route),
   containedIn(Towns, Route).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3. Find a route that can be used by a list of travellers %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The predicate gets the number of Travellers
% It gets alist of vehicles that can carry the given number of travellers
% It gets the list of vehicles each traveller can travel with. This is a list of lists
% Find the intersection of that list to to find out what vehicles can all travellers use
% Gets the length of the common vehicles list and checks that it is not empty
% If its not empty it checks that the common vehicles is in the list of the vehicles that can carry the given number of people
% If yes it get the road types that the common vehicles can use
% Finally it gets the route between the origin and destination based on the road types
pathWithTravellers(Origin, Destination, Travellers, Route):-
   length(Travellers, NumberOfTravellers),
   setof(Vehicle, vechicleCapacity(Vehicle, NumberOfTravellers), VehiclesThatCanCarry),
   maplist(traveller, Travellers, ListOfAvailableVehicles), 
   intersection(ListOfAvailableVehicles, CommonVehicles),
   length(CommonVehicles, NumberOfCommonVehicles),
   NumberOfCommonVehicles > 0,
   containedIn(CommonVehicles, VehiclesThatCanCarry),
   setof(RoadType, vehiclesRoadType(CommonVehicles, RoadType), RoadTypes),
   pathWithRoadTypes(Origin, Destination, RoadTypes, Route).
  
% How many people can the given vehicle carry
vechicleCapacity(Vehicle, NumberOfTravellers):-
   vehicle(Vehicle, Capacity),
   Capacity >= NumberOfTravellers.

% Find the common elements in two lists
intersection([Head|Tail], Result):-
   intersectionHelper(Tail, Head, Result).

% Helper for the intersection function
% Uses recursion.
intersectionHelper([], Result, Result).
intersectionHelper([Head|Tail], Intermediate, Result) :-
  intersection(Head, Intermediate, NewIntermediate),
  intersectionHelper(Tail, NewIntermediate, Result).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4. Reqirement from miniproject: Find a route that uses only certain road conditions %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Gets a path between origin and destination for specified road types
pathWithRoadTypes(Origin, Destination, RoadTypes, Route):-
  pathWithRoadTypes_helper(Origin, Destination, RoadTypes, [Origin], Route).

% A helper to the above function
% It takes a path between origin and destination and checks the road type between the two
% If the road type is in the list of road types it appends it to the list
% If there is no direct connection between origin and destination it checks the raod type between origin and a town in between origin and destination
% If that road type is in the list of road types it append to the town to the results
% It also checks if this intermediate town is already in the current town list and that the destination is not in the list yet. 
pathWithRoadTypes_helper(Origin, Destination, RoadTypes, CurrentRoute, Result) :-
  way(Origin, Destination, _, RoadType, _), 
  isInList(RoadType, RoadTypes),
  appendToList(CurrentRoute, Destination, Result).
pathWithRoadTypes_helper(Origin, Destination, RoadTypes, CurrentRoute, Result) :-
  way(Origin, Intermediate, _, RoadType, _),
  isInList(RoadType, RoadTypes),
  notInList(Intermediate, CurrentRoute), 
  notInList(Destination, CurrentRoute), 
  appendToList(CurrentRoute, Intermediate, UpdatedCurrentRoute),
  pathWithRoadTypes_helper(Intermediate, Destination, RoadTypes, UpdatedCurrentRoute, Result).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5. Find the shortest route between two given towns %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Test with allborg - copenhagen %%
% Takes an origin a destinations and returns the shortes route between the two.
% First it gets a list of all possible routs using the previously defined path(rigin, Destination, Route) predicate
% Than it uses maplist and calculates the length of each pissible route and gets a list of the distances
% Then it merges the route list with the appropriate distance
% Finally, it selects the shortest routes
shortestRoute(Origin, Destination, ShortestRoute) :-
  setof(Route, path(Origin, Destination, Route), PossibleRoutes),
  maplist(routeLength, PossibleRoutes, RouteLengths), 
  mergeLists(PossibleRoutes, RouteLengths, RouteDistance),
  write(RouteDistance),
  smallestDistanceRoute(RouteDistance, ShortestRoute).

% Calculates the lengths of a route
% It gets a list of the direct connections between origin and destination
% Gets the distances between the direct connections
% Sums up the obtained distances
routeLength(Route, Lenght) :-
  directPaths(Route, Directpaths),
  maplist(directDistance, Directpaths, Distances),
  sumlist(Distances, Lenght).

% Gets a list of direct paths between origin and destination
% For example [aalborg, randers, aarhus, copenhagen] --> [[aalborg, randers], [randers, aarhus], [aarhus, copenhagen]] 
% Follows the same pattern as the pther recursive functions and uses a helper
directPaths(Route, Roads) :-
  directPathsHelper(Route, [], Roads).

% If the tail is empty the path is already direct
% If the tail is not empty  it takes the head of the tail because that will be the direct destination
% Then appends it to the list then calls directPathsHelper recursively
directPathsHelper([_|[]], Roads, Roads).
directPathsHelper([Origin|Rest], Intermediate, Roads) :-
  head(Rest, Destination),
  appendToList(Intermediate, [Origin,Destination], NewIntermediate),
  directPathsHelper(Rest, NewIntermediate, Roads).

% Gets the distance of a direct path
directDistance([Origin, Destination], Distance) :-
  way(Origin, Destination, Distance, _, _),
  Distance = Distance.

% Merges two list so that it adds the first element of the second list to the first element of first list, etc.
% Example [aalborg, randers, aarhus], [1, 2 ,3] --> [[aalborg, 1], [randers, 2], [aarhus, 3]]
mergeLists(List1, List2, Result) :-
  mergeListsHelper(List1, List2, [], Result).

mergeListsHelper([], [], Result, Result).
mergeListsHelper([Head1|Tail1], [Head2|Tail2], Intermediate, Result) :-
  appendToList([], Head1, L1),
  appendToList(L1, Head2, L2),
  appendToList(Intermediate, L2, NewIntermediate),
  mergeListsHelper(Tail1, Tail2, NewIntermediate, Result).

% Selects the smallest distance
% Uses helper for recursion
smallestDistanceRoute([[Route, Length]|Rest], Result) :-
  smallestDistanceRouteHelper(Rest, Route, Length, Result).

% The list is of form [[Route, Lengths], [Rpute, Length], ...]
% So we can get the length and compare them
% If the length is smaller that the CurrentLength we compare length to the Rest recursively
% If it bigger we compare CurrentLength to the rest recursively
smallestDistanceRouteHelper([], Result, _, Result).
smallestDistanceRouteHelper([[Route, Length]|Rest], CurrentRoute, CurrentLength, Result) :-
  Length < CurrentLength,
  smallestDistanceRouteHelper(Rest, Route, Length, Result);
  Length >= CurrentLength,
  smallestDistanceRouteHelper(Rest, CurrentRoute, CurrentLength, Result).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6. Find the fastest route between two given towns %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Same idea as for requirement number 5 but instead of comparing distances we compare travel times
fastestRoute(Origin, Destination, FastestRoute) :-
  setof(Route, path(Origin, Destination, Route), PossibleTimes),
  maplist(routeTime, PossibleTimes, RouteTimes),  
  mergeLists(PossibleTimes, RouteTimes, RoutesWithTimes),
  write(RoutesWithTimes),
  smallestTimeRoute(RoutesWithTimes, FastestRoute).

% Calculates how long is the travel time of the route
routeTime(Route, Time) :-
  directPaths(Route, Directpaths),
  maplist(directTime, Directpaths, TravelTime),
  sumlist(TravelTime, Time).

% Finds the travel time between direct paths
directTime([Origin, Destination], Time) :-
  way(Origin, Destination, _, _, TravelTime),
  Time = TravelTime.

% Same idea as with the smallestDistanceRoute and smallestDistanceRouteHelper
smallestTimeRoute([[Route, Length]|Rest], Result) :-
  smallestTimeRouteHelper(Rest, Route, Length, Result).

smallestTimeRouteHelper([], Result, _, Result).
smallestTimeRouteHelper([[Route, TravelTime]|Rest], CurrentRoute, CurrentTravelTime, Result) :-
  TravelTime < CurrentTravelTime,
  smallestTimeRouteHelper(Rest, Route, TravelTime, Result);
  TravelTime >= CurrentTravelTime,
  smallestTimeRouteHelper(Rest, CurrentRoute, CurrentTravelTime, Result).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 7. Tell us, given a traveller, which means of transportation (if any) will allow them to get from a to b %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The predicate takes an origin, a destination, a traveller and returns a list of vehicles that the given traveller can use on the requested route
% First it gets all the availabe vehiclesa traveller can travel by
% Then it gets the availbe routs for that traveller between origin and destination using the predicate from requiremnt 1
% Than it maps the vehicles available for the routes to the available routes
% Then it flattens the list of vehicles
% Then it uses intersection to find the common elemnts in the flattened list and the list of vehicles the traveller can use
% Finally, it returns the result of that intersection
vehiclesForTraveller(Origin, Destination, Traveller, VehiclesForTraveller) :-
  traveller(Traveller, TravellerVehicles),
  setof(Route, personalRoute(Origin, Destination, Traveller, Route), AvailableRoutes),
  maplist(vehiclesForRoute, AvailableRoutes, VehicleLists),
  flattenList(VehicleLists, FlatVehicleList),
  intersection(TravellerVehicles, FlatVehicleList, Vehicles),
  VehiclesForTraveller = Vehicles.

% It gets the available vehicles for a given route
% To do this first it gets the direct paths inside route
% Then it maps the possible road type for each direct path
% Becasue there can be repeating road types it removes the duplicate elements
% Then it maps the vehicles that can be used to the given road type to unique road types
% Then it flattens the list and removes the duplicate elements
% Finally it returns a list of vehicles that are possible to use for the route
vehiclesForRoute(Route, Vehicles) :-
  directPaths(Route, Roads),
  maplist(roadTypeForDirect, Roads, RoadTypes),
  listToUniqueList(RoadTypes, UniqueRoadTypes),
  maplist(vehiclesForRoadType, UniqueRoadTypes, ListOfVehicleLists),
  flattenList(ListOfVehicleLists, FlatList),
  listToUniqueList(FlatList, UniqueVehicles),
  Vehicles = UniqueVehicles.

% Takes a direct path and gets the type of the road for that direct path
roadTypeForDirect([Origin, Destination], RoadType) :-
  way(Origin, Destination, _, TypeOfRoad, _),
  RoadType = TypeOfRoad.

% Creates a list of all the vehicles that can be used on the road type
vehiclesForRoadType(RoadType, Vehicles) :-
  setof(VehicleType, vehicleRoadType(VehicleType, RoadType), Vehicles).

vehicleRoadType(Vehicle, RoadType):- vehiclesRoadType([Vehicle], RoadType).

%%%%%%%%%%%%%%%%%%%%%%%%
% Other used predicates%
%%%%%%%%%%%%%%%%%%%%%%%%

% Appends an element to a list.
% If the list is the empty list it simply adds the element to the list
% If the list is not empty it adds the element to the end
appendToList([],Elem,[Elem]).
appendToList([Head|Tail],Elem,[Head|NewTail]) :-
  appendToList(Tail, Elem, NewTail).

% Checks if an element is in the list
isInList(_, []) :- fail.
isInList(Elem, [Head|Tail]) :- Elem == Head; isInList(Elem, Tail).

% Checks if an element is not in the list
notInList(Elem, List):- not(isInList(Elem, List)).

% Checks if the first list is in the second list
containedIn([], _) :- fail.
containedIn(L1, L2) :- maplist(contains(L2), L1).
contains(L, X) :- member(X, L).

% Append element to the list only if the element is not in the list already
% If the element is already in the list do not append to it
appendUnique(List, Elem, Res) :-
  notInList(Elem, List), 
  appendToList(List, Elem, Res).

appendUnique(List, Elem, Res) :-
  isInList(Elem, List),
  Res = List.

listToUniqueList(List, Result) :-
  listToUniqueListHelper(List, [], Result).

listToUniqueListHelper([], Result, Result).
listToUniqueListHelper([Head|Tail], Intermediate, Result) :-
  appendUnique(Intermediate, Head, NewIntermediate),
  listToUniqueListHelper(Tail, NewIntermediate, Result).

% Gets the head of the list
% Used to get the first element of the tail in requirement 5
head([Head|_], Head).
tail([_|Tail], Tail).

% Flattens a list
% If the list is empty there is nothing to flatten
% If the list is not empty it calls it self on the head of the list
% Then it calls itself recursively on the tail
% it appends the the results
flattenList([], []) :- !.
flattenList([L|Ls], FlatL) :-
    !,
    flattenList(L, NewL),
    flattenList(Ls, NewLs),
    append(NewL, NewLs, FlatL).
flattenList(L, [L]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% Did not use in final solution%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This one is problematic - while it gets the distance it does not stop even after it reached the destination.
% Returns a path between two towns and the distance between them
% It considers three scenarios:
% 1. There is a direct connection between the towns.
% 2. There is a connection in the form of O-X-D - one town between origin and Destination
% 3. More than one town between origin and destination
pathDistance(Origin, Destination, Path, Distance):-
   way(Origin, Destination, Distance, _, _), Path = [Origin, Destination];
   way(Origin, PassThroughTown, Distance1, _, _), way(PassThroughTown, Destination, Distance3, _, _), Path=[Origin, PassThroughTown, Destination], plus(Distance1, Distance3, Distance);
   way(Origin, PassThroughTown, Distance1, _, _), way(OtherPassThrough, Destination, Distance3, _, _), pathDistance(PassThroughTown, OtherPassThrough, P2, Distance2),
      appendUnique([Origin|P2], Destination, Path),
         plus(Distance1, Distance2, DistanceA),
            plus(DistanceA, Distance3, Distance).

% Gets the last element of the list
lastElement([X], Element) :- Element = X.
lastElement([X|Y], Element) :- lastElement(Y, Element).

